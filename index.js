const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup', concatFirstLastName)

txtLastName.addEventListener('keyup', concatFirstLastName)

function concatFirstLastName (event) {
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}